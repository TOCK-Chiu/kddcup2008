# 
# KDDCup2008
# 





CC       = gcc
CXX      = g++
CFLAGS   = -std=c11 -pedantic
CXXFLAGS = -std=c++11 -Wall -Wextra -pedantic
LDFLAGS  = 
OBJ_DIR  = obj
SRC      = $(wildcard src/*.cpp)
OBJ      = ${SRC:src/%.cpp=$(OBJ_DIR)/%.o}
TARGET   = Classifier
TEMP_NET = temp.net



ifeq ($(MAKECMDGOALS),debug)
	CFLAGS   += -Og -g3 -fsanitize=address
	CXXFLAGS += -Og -g3 -fsanitize=address
	LDFLAGS  +=
else
	CFLAGS   += -flto -O2
	CXXFLAGS += -flto -O2
	LDFLAGS  += -flto -O2 -s
endif





all: $(TARGET)

debug: $(TARGET)

release: $(TARGET)

$(TARGET): $(OBJ) $(OBJ_DIR)/doublefann.o | $(OBJ_DIR)
	$(CXX) $^ $(CXXFLAGS) $(LDFLAGS) -o $(TARGET)

$(OBJ_DIR)/%.o: src/%.cpp | $(OBJ_DIR)
	$(CXX) -Ifann-src/src/include $(CXXFLAGS) -c $< -o $@

# I don't want to build the whole library.
$(OBJ_DIR)/doublefann.o: fann-src/src/doublefann.c | $(OBJ_DIR)
	$(CC) -Ifann-src/src/include $(CFLAGS) -c $< -o $@

$(OBJ_DIR): 
	mkdir -p $(OBJ_DIR)

.PHONY: clean all debug release test

test: all
	@echo -e "\n====================\nRun training...\n====================\n"
	@./$(TARGET) train testcase/TrainingInfo.txt testcase/TrainingFeatures.txt $(TEMP_NET)
	@echo -e "\n====================\nRun testing...\n====================\n"
	@./$(TARGET) test testcase/TestingInfo.txt testcase/TestingFeatures.txt $(TEMP_NET)
	@$(RM) -f $(TEMP_NET)

clean:
	$(RM) -rf $(TARGET) $(OBJ_DIR)
