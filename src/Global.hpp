/*

  Global.hpp

  A header for checking language requirement or so.

*/





#ifndef __CLASSIFIER__GLOBAL_HPP__
#define __CLASSIFIER__GLOBAL_HPP__

#if !defined(__cplusplus) || __cplusplus < 201103L
#error "C++11 is required!"
#endif

#endif
