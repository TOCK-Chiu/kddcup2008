/*

  FANNCallback.cpp

*/

#include <cstring>
#include <ctime>
#include <utility>
#include <algorithm>
#include "Global.hpp"
#include "FANNCallback.hpp"





fann_user::op_t fann_user::Operation;
FILE* fann_user::InfoFile = nullptr;
FILE* fann_user::FeaturesFile = nullptr;
size_t fann_user::NumberOfData = 0;
size_t fann_user::NumberOfPositiveData = 0;
size_t fann_user::NumberOfNegativeData = 0;



// Simple callback function to create a train data.
void FANN_API fann_user::SimpleReader(unsigned int CurrentIndex,
                                      unsigned int NumberOfInput,
                                      unsigned int NumberOfOutput,
                                      double* TrainingInput,
                                      double* TrainingOutput){

	(void) CurrentIndex;
	(void) NumberOfInput;
	(void) NumberOfOutput;

	double Temp;
	size_t InfoPerRow = ( Operation == RUN ) ? 8 : 11;

	for(size_t Index = 0; Index < 117; Index++){

		if( fscanf(FeaturesFile, " %lf", &TrainingInput[Index]) != 1 )
			throw std::make_pair<const char*, const char*>("fann_user::SimpleReader", "bad features file.");

		}

	if( fscanf(InfoFile, " %lf", &Temp) != 1 )
		throw std::make_pair<const char*, const char*>("fann_user::SimpleReader", "bad info file.");

	// Note: These for running mode are meaningless. And will be ignored.
	//       Whatsoever, I just don't want to rewrite these reading code.
	//       The performance impact should not be significant either.
	TrainingOutput[0] = 1;
	TrainingOutput[1] = -1;

	// Swap them if it's a positive sample.
	if( Temp > 0 )
		std::swap(TrainingOutput[0], TrainingOutput[1]);

	// Discard remain inputs.
	for(size_t Index = 1; Index < InfoPerRow; Index++){

		if( fscanf(InfoFile, " %lf", &Temp) != 1 )
			throw std::make_pair<const char*, const char*>("fann_user::SimpleReader", "bad info file.");

		}

	}



// Resample callback function to create a train data.
void FANN_API fann_user::ResampleReader(unsigned int CurrentIndex,
                                        unsigned int NumberOfInput,
                                        unsigned int NumberOfOutput,
                                        double* TrainingInput,
                                        double* TrainingOutput){

	static double MagicBox[119];
	static double MagicCounter = 0;
	static size_t PositiveCounter = 0;
	static size_t NegativeCounter = 0;
	static std::mt19937_64 Rand( time(nullptr) );
	static std::uniform_real_distribution<double> Distribute(0, 1);

	if( MagicCounter > Distribute(Rand) ){

		memcpy(TrainingInput, MagicBox, sizeof(double) * 117);
		memcpy(TrainingOutput, MagicBox+117, sizeof(double) * 2);
		MagicCounter -= 1;
		return ;

		}

	SimpleReader(CurrentIndex, NumberOfInput, NumberOfOutput, MagicBox, MagicBox+117);

	// Negative.
	if( MagicBox[117] < 0 && PositiveCounter < NumberOfPositiveData ){

		MagicCounter = static_cast<double>( NumberOfData / 2 ) / ( NumberOfPositiveData - PositiveCounter );
		PositiveCounter++;

		}
	// Positive otherwise.
	else if( NegativeCounter < NumberOfNegativeData ){

		MagicCounter = static_cast<double>( NumberOfData / 2 ) / ( NumberOfNegativeData - NegativeCounter );
		NegativeCounter++;

		}

	ResampleReader(CurrentIndex, NumberOfInput, NumberOfOutput, MagicBox, MagicBox+117);

	}



// Call back reporter.
int FANN_API fann_user::Reporter(fann* Classifier,
                                 fann_train_data* TrainingData,
                                 unsigned int MaxEpoch,
                                 unsigned int EpochBetweenReport,
                                 float DesiredError,
                                 unsigned int Epoch){

	(void) TrainingData;
	(void) EpochBetweenReport;

	if( Epoch == 1 )
		fprintf(stderr, "Max epoch = %u, Target MSE = %.7f\n", MaxEpoch, DesiredError);

	fprintf(stderr, "Epoch #%u\tMSE = %.7f\n", Epoch, fann_get_MSE(Classifier));

	return 0;

	}
