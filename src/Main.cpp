#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <limits>
#include <algorithm>
#include <utility>
#include "Global.hpp"
#include "FANNCallback.hpp"
#include "doublefann.h"





////////////////////////////////////////
// Global function definition.
////////////////////////////////////////

// Print usage.
void PrintUsage(const char* This){

	fprintf(stderr, "\nUsage:\n");
	fprintf(stderr, "   \'%s\' <operation> <info> <features> <network>\n", This);

	fprintf(stderr, "\nOptions:\n");
	fprintf(stderr, "   <operation> ........ Must be \'train\' or 'test'.\n");
	fprintf(stderr, "   <info> ............. Specify additional information descrided in KDDCup2008.\n");
	fprintf(stderr, "   <features> ......... Specify training dataset described in KDDCup2008.\n");
	fprintf(stderr, "   <network> .......... Network file to gen or use, depending on the operation.\n");

	fprintf(stderr, "\nExample:\n");
	fprintf(stderr, "   \'%s\' train info.txt features.txt kdd_cup_2008.net\n", This);

	exit(0);

	}



// Count number of data in the info file.
size_t CountNumberOfData(FILE* FileToCount,
                         size_t NumberOfAttribute,
                         bool IsCountPN,
                         size_t* PtrNumberOfPositive,
                         size_t* PtrNumberOfNegative){

	size_t NumberOfData = 0;
	size_t NumberOfPositive = 0;
	double Temp;

	// Read data and count the number of P/N if needed.
	while( fscanf(FileToCount, " %lf", &Temp) == 1 ){

		// Extract first attribute.
		// The first attribute in each row indicates whether it's positive.
		if( IsCountPN && ! ( NumberOfData % NumberOfAttribute ) ){

			// Only count the number of positive.
			// The number of negative sample shall be much greater than positive one.
			if( Temp > 0 )
				NumberOfPositive++;

			}

		NumberOfData++;

		}

	// If I/O error happened.
	if( ferror(FileToCount) )
		throw std::make_pair<const char*, const char*>("CountNumberOfData", "failed to read info file.");

	// If magic data appear.
	if( ! feof(FileToCount) || ( NumberOfData % NumberOfAttribute ) )
		throw std::make_pair<const char*, const char*>("CountNumberOfData", "bad input file.");

	rewind(FileToCount);

	// Record the number.
	if( IsCountPN ){

		*PtrNumberOfPositive = NumberOfPositive;
		*PtrNumberOfNegative = NumberOfData - NumberOfPositive;

		}

	return ( NumberOfData / NumberOfAttribute );

	}





////////////////////////////////////////
// Main function.
////////////////////////////////////////
int main(int ArgumentCount, const char* Argument[]){

	size_t InfoPerRow = 11;

	// Print usage if the number of arguments is wrong.
	if( ArgumentCount != 5 )
		PrintUsage(Argument[0]);

	// Check parameter before running.
	if( ! strcmp(Argument[1], "train") )
		fann_user::Operation = fann_user::TRAIN;
	else if( ! strcmp(Argument[1], "test") )
		fann_user::Operation = fann_user::TEST;
	// Info file lacks the first three value in real testing data.
	else if( ! strcmp(Argument[1], "run") ){

		fann_user::Operation = fann_user::RUN;
		InfoPerRow = 8;

		}
	// Print usage for unknown operation.
	else
		PrintUsage(Argument[0]);

	// FANN depends on rand.
	srand( time(nullptr) );

	try{

		// Things start here.
		fann* Classifier = nullptr;
		fann_train_data* Data = nullptr;

		fann_user::InfoFile = fopen(Argument[2], "r");
		fann_user::FeaturesFile = fopen(Argument[3], "r");

		// Checking file status.
		if( ! fann_user::InfoFile || ! fann_user::FeaturesFile )
			throw std::make_pair<const char*, const char*>("main", "failed to open input file.");

		fprintf(stderr, "Counting...\n");

		// Getting the number of data.
		fann_user::NumberOfData = CountNumberOfData(fann_user::InfoFile, InfoPerRow, true, &fann_user::NumberOfPositiveData, &fann_user::NumberOfNegativeData);

		if( fann_user::NumberOfData != CountNumberOfData(fann_user::FeaturesFile, 117, false, nullptr, nullptr) )
			throw std::make_pair<const char*, const char*>("main", "number of data unmatched!");

		fprintf(stderr, "Reading...\n");

		// Training mode.
		if( fann_user::Operation == fann_user::TRAIN ){

			fann_user::NumberOfData = 2 * sqrt( fann_user::NumberOfPositiveData * fann_user::NumberOfNegativeData );
			Data = fann_create_train_from_callback(fann_user::NumberOfData, 117, 2, fann_user::ResampleReader);
			Classifier = fann_create_standard(3, 117, 22, 2);

			if( !Data || !Classifier )
				throw std::make_pair<const char*, const char*>("main", "failed to create a new network or read training data.");

			fprintf(stderr, "Shuffling...\n");

			fann_set_activation_function_hidden(Classifier, FANN_SIGMOID_SYMMETRIC);
			fann_set_activation_function_output(Classifier, FANN_SIGMOID_SYMMETRIC);
			fann_set_training_algorithm(Classifier, FANN_TRAIN_RPROP);
			fann_set_callback(Classifier, fann_user::Reporter);
			fann_shuffle_train_data(Data);
			fann_set_scaling_params(Classifier, Data, -1, 1, -1, 1);

			fprintf(stderr, "Training...\n");

			fann_train_on_data(Classifier, Data, 512, 1, 0.01);

			fprintf(stderr, "Saving...\n");

			if( fann_save(Classifier, Argument[4]) )
				throw std::make_pair<const char*, const char*>("main", "failed to save network.");

			}
		// Test or running mode.
		else{

			Data = fann_create_train_from_callback(fann_user::NumberOfData, 117, 2, fann_user::SimpleReader);
			Classifier = fann_create_from_file(Argument[4]);

			if( !Data || !Classifier )
				throw std::make_pair<const char*, const char*>("main", "failed to create network from file or read test data.");

			// Testing mode.
			if( fann_user::Operation == fann_user::TEST ){

				size_t TP = 0;
				size_t TN = 0;
				size_t FP = 0;
				size_t FN = 0;

				fprintf(stderr, "Testing...\n");

				for(size_t Index = 0; Index < fann_user::NumberOfData; Index++){

					fann_scale_input(Classifier, Data->input[Index]);
					double* Output = fann_run(Classifier, Data->input[Index]);

					// Negative.
					if( Data->output[Index][0] > 0 ){

						if( Output[0] > Output[1] )
							TN++;
						else
							FP++;

						}
					// Positive.
					else{

						if( Output[0] > Output[1] )
							FN++;
						else
							TP++;

						}

					}

				size_t Total = TP + TN + FP + FN;

				fprintf(stderr, "\ntotal = %zu\n", Total);
				fprintf(stderr, "ACC = %.2lf%%\n", static_cast<double>((TP+TN)*100)/Total);
				fprintf(stderr, "TPR = %.2lf%%\n", (TP*100)/static_cast<double>(TP+FN));
				fprintf(stderr, "TNR = %.2lf%%\n", (TN*100)/static_cast<double>(FP+TN));
				fprintf(stderr, "PPV = %.2lf%%\n", (TP*100)/static_cast<double>(TP+FP));
				fprintf(stderr, "NPV = %.2lf%%\n", (TN*100)/static_cast<double>(TN+FN));
				fprintf(stderr, "MCC = %.2lf\n", (TP*TN-FP*FN)/sqrt(static_cast<double>((TP+FP)*(TP+FN)*(TN+FP)*(TN+FN))));

				fprintf(stderr, "\nreal\\guess\tnegative\tpositive\n");
				fprintf(stderr, "negative\t%zu(%.2lf%%)\t%zu(%.2lf%%)\n", TN, static_cast<double>(TN*100)/Total, FP, static_cast<double>(FP*100)/Total);
				fprintf(stderr, "positive\t%zu(%.2lf%%)\t%zu(%.2lf%%)\n", FN, static_cast<double>(FN*100)/Total, TP, static_cast<double>(TP*100)/Total);

				}
			// Running mode.
			else{

				fprintf(stderr, "Running...\n");

				for(size_t Index = 0; Index < fann_user::NumberOfData; Index++){

					fann_scale_input(Classifier, Data->input[Index]);
					double* Output = fann_run(Classifier, Data->input[Index]);

					// Print to stdout.
					// Users can redirect the result to stdout.
					if( Output[0] > Output[1] )
						fprintf(stdout, "negative\n");
					else
						fprintf(stdout, "positive\n");

					}

				}

			}

		fann_destroy_train( Data );
		fann_destroy(Classifier);
		fclose(fann_user::InfoFile);
		fclose(fann_user::FeaturesFile);

		}
	// Catch standard exception.
	catch(const std::exception& Exception){

		fprintf(stderr, "std::exception: %s\n", Exception.what());
		exit(-1);

		}
	// Custom exception.
	catch(const std::pair<const char*, const char*>& Exception){

		fprintf(stderr, "%s: %s\n", Exception.first, Exception.second);
		exit(-1);

		}
	// Unknown exception.
	catch(...){

		fprintf(stderr, "Unknown exception is caught. This may be caused by a bug.\n");
		exit(-1);

		}

	exit(0);

	}
