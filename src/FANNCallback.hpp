/*

  FANNCallback.hpp

*/





#ifndef __CLASSIFIER__FANN_CALLBACK_HPP__
#define __CLASSIFIER__FANN_CALLBACK_HPP__

#include <cstdio>
#include "Global.hpp"
#include "doublefann.h"





namespace fann_user{

	typedef enum { TRAIN, TEST, RUN } op_t;

	extern op_t Operation;
	extern FILE* InfoFile;
	extern FILE* FeaturesFile;
	extern size_t NumberOfData;
	extern size_t NumberOfPositiveData;
	extern size_t NumberOfNegativeData;

	void FANN_API SimpleReader(unsigned int CurrentIndex,
	                           unsigned int NumberOfInput,
	                           unsigned int NumberOfOutput,
	                           double* TrainingInput,
	                           double* TrainingOutput);

	void FANN_API ResampleReader(unsigned int CurrentIndex,
	                             unsigned int NumberOfInput,
	                             unsigned int NumberOfOutput,
	                             double* TrianingInput,
	                             double* TrianingOutput);

	int FANN_API Reporter(fann* Classifier,
	                      fann_train_data* TrainingData,
	                      unsigned int MaxEpoch,
	                      unsigned int EpochBetweenReport,
	                      float DesiredError,
	                      unsigned int Epoch);

	}





#endif
